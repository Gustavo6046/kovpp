#pragma once

#include <string>
#include <cassert>



template <typename T, unsigned char order>
class KovNode;

template <typename T, unsigned char order>
class KovChain;


template <typename T>
class KovDataAppender {
public:
    T *(*Append)(T &a, T &b);
    KovDataAppender(T *(*initAppend)(T &a, T &b)) : Append(initAppend) {}
};


template <typename T, unsigned char order>
class KovData {
    std::shared_ptr<T> data;
    KovDataAppender<T> *appender;
    
public:
    KovData<T, order> operator+(KovNode<T, order> &other) {
        if (!data) {
            assert(other.GetWrappedData() != NULL);
            return *other.GetWrappedData();
        }

        return Append(other.GetWrappedData().get());
    }

    KovData<T, order> operator+(KovData<T, order> &other) {
        if (!data) return other;

        return Append(&other);
    }

    KovData(KovDataAppender<T> *newAppender) : appender(newAppender), data(NULL) {}
    KovData(KovDataAppender<T> *newAppender, T &initial) : appender(newAppender), data(&initial) {}
    KovData(KovDataAppender<T> *newAppender, T *initial) : appender(newAppender), data(initial) {}
    KovData(const KovData &other) : data(other.data), appender(other.appender) {}

    T &GetData(void);
    bool Valid(void);

private:
    KovData<T, order> Append(KovData<T, order> *other);
};

extern KovDataAppender<std::string> KovStringInit;



//----------------//
// IMPLEMENTATION //
//----------------//

#include <string>
#include <sstream>



template <typename T, unsigned char order>
T &KovData<T, order>::GetData(void) {
    assert(data != NULL);
    return *data;
}


template <typename T, unsigned char order>
bool KovData<T, order>::Valid(void) {
    return data != NULL;
}


template <typename T, unsigned char order>
KovData<T, order> KovData<T, order>::Append(KovData<T, order> *other) {
    if (!data) {
        assert(other != NULL);
        return *other;
    }

    if (!other || !other->data)
        return *this;

    T *resData = (appender->Append)(*data, *other->data);
    KovData<T, order> res(appender, resData);

    return res;
}



#include <iostream>

std::string *appendStrings(std::string &a, std::string &b) {
    std::stringstream ss;
    ss << a << b;

    return new std::string(ss.str());
}

KovDataAppender<std::string> KovStringInit(&appendStrings);